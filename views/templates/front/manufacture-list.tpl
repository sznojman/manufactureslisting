{*{$siema}*}
<div>
    <div>
        <ul>
           {foreach from=$manufacturers item=manufacturer name=manufacturers}
                <li>
                    <a href="/producent/{$manufacturer.id_manufacturer}">
                        {$manufacturer.name} ({$manufacturer.nb_products})
                    </a>
                </li>
           {/foreach}
        </ul>
    </div>
</div>