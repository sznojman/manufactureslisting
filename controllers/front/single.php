<?php
/**
 * Created by PhpStorm.
 * User: sznojman
 * Date: 12.10.17
 * Time: 17:36
 */

class ManufactureslistingSingleModuleFrontController extends ModuleFrontController{

	public function initContent()
	{
		parent::initContent();

		$id = Tools::getValue('id_manufacturer');

		$manufacturer = new Manufacturer((int)$id);
		$products = Manufacturer::getProducts(
			$manufacturer->id_manufacturer,
			$this->context->language->id,
			0,
			9
		);
//		var_dump($products);
		$this->context->smarty->assign(array(
			'siema' => $id,
			'products' => $products
		));

		$this->setTemplate('manufacture-single.tpl');
	}


}