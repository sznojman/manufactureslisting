<?php
/**
 * Created by PhpStorm.
 * User: sznojman
 * Date: 11.10.17
 * Time: 21:18
 */

class ManufactureslistingListModuleFrontController extends ModuleFrontController{

	public function initContent()
	{
		parent::initContent();

		$manufacturers = Manufacturer::getManufacturers(
			10,
			$this->context->language->id,
			$active = true,
			$p = false,
			$n = false,
			$all_group = false,
			$group_by = false
		);
//		var_dump($manufacturers);

		$this->context->smarty->assign([
//
			'manufacturers' => $manufacturers
		]);

		$this->setTemplate('manufacture-list.tpl');
	}

}