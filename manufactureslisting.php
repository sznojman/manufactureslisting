<?php
/**
 * Created by PhpStorm.
 * User: sznojman
 * Date: 11.10.17
 * Time: 20:43
 */

class manufactureslisting extends Module {
	public function __construct() {
		$this->name                   = 'manufactureslisting';
		$this->tab                    = 'front_office_features';
		$this->version                = '1.0.0';
		$this->author                 = 'Firstname Lastname';
		$this->need_instance          = 0;
		$this->ps_versions_compliancy = array( 'min' => '1.6', 'max' => _PS_VERSION_ );
		$this->bootstrap              = true;
		parent::__construct();
		$this->displayName = $this->l( 'manufactureslisting' );
		$this->description = $this->l( 'Description of my module.' );
		$this->confirmUninstall = $this->l( 'Are you sure you want to uninstall?' );
	}

	public function install() {
		if ( ! parent::install() || !$this->registerHook('moduleRoutes') ) {
			return false;
		}

		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall()) {
			return false;
		}

		return true;
	}


	public function hookModuleRoutes() {
		return array(
			'manufactureslisting-list' => array(
				'controller' => 'list',
				'rule' => 'lista-producentow', //the desired page URL contains parameters
				'keywords' => array(),
				'params' => array(
					'fc' => 'module',
					'module' => 'manufactureslisting', //module name
				)
			),
			'manufactureslisting-single' => array(
				'controller' => 'single',
				'rule' => 'producent/{id}', //the desired page URL contains parameters
				'keywords' => array( //in the Keywords section we describe every parameter
					'id' => array('regexp' => '[0-9]+', 'param' => 'id_manufacturer')
				),
				'params' => array(
					'fc' => 'module',
					'module' => 'manufactureslisting', //module name
				)
			)
		);
	}

}